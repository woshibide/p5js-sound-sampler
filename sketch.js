// ----------------- class -----------------

class samplerTab {
  constructor(x, y, w, h, audioFile, color, text, sKey = null) { 
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.audioFile = audioFile;
    this.color = color;
    this.text = text;
    this.sKey = sKey;
    this.isPressed = false;
  }

  draw() {
    // shape the UI
    ellipse(this.x, this.y, this.w, this.h);
    textAlign(CENTER, CENTER);
    
    // text for name property
    textSize(16); 
    text(this.text, this.x, this.y + 40); 
    
    // text for shortcut property 
    fill(0, 0, 0);
    textSize(12);
    text(`${this.sKey}`, this.x, this.y); 
    
    // adjust color based on whether button is pressed or not
    // for some miraculous reason it shades next item on the list
    if (this.isPressed) {
      fill(this.color.levels.map(x => x - 50)); // darker shade when pressed
    } else {
      fill(this.color);
    }
  }

  isMouseOver() {
    let d = dist(mouseX, mouseY, this.x, this.y);
    return d < this.w / 2;
  }

  playSound() {
    this.audioFile.play();
  }

  press() {
    // clear any existing timers for this button
    clearTimeout(this.timerId);
    this.isPressed = true;
    // set a new timer to reset the button color
    this.timerId = setTimeout(() => this.isPressed = false, 10000); 
  }  
}

// ----------------- p5js -----------------

// list to store buttons 
let buttons = [];

function preload() {
  // place audio files inside assets folder 
  // and update this part with new filenames
  audio0 = loadSound('assets/sound1.wav');
  audio1 = loadSound('assets/sound2.wav');
  audio2 = loadSound('assets/sound3.wav');
  audio3 = loadSound('assets/sound4.wav');
  audio4 = loadSound('assets/sound5.wav');
  audio5 = loadSound('assets/sound6.wav');
}

function setup() {
  createCanvas(400, 400);
  
  buttons.push(new samplerTab(100, 150, 50, 50, audio0, color('white'), "sound 1", "Q"));
  buttons.push(new samplerTab(200, 150, 50, 50, audio1, color('white'), "sound 2", "W"));
  buttons.push(new samplerTab(300, 150, 50, 50, audio2, color('white'), "sound 3", "E"));
  buttons.push(new samplerTab(100, 250, 50, 50, audio3, color('white'), "sound 4", "A"));
  buttons.push(new samplerTab(200, 250, 50, 50, audio4, color('white'), "sound 5", "S"));
  buttons.push(new samplerTab(300, 250, 50, 50, audio5, color('white'), "sound 6", "D"));
}

function draw() {
  background(12, 91, 212);

  // draw buttons on the screen
  for (let i = 0; i < buttons.length; i++) {
    buttons[i].draw();
  }
}

function keyPressed() {
  for (let i = 0; i < buttons.length; i++) {
    if (key.toUpperCase() === buttons[i].sKey) {
      console.log(`Pressed key: ${key}`);
      console.log(`Button text: ${buttons[i].text}`);
      console.log(`Button index: ${i}`);
      console.log('---');
      buttons[i].playSound();
      buttons[i].press();
    }
  }
}

function mousePressed() {
  for (let i = 0; i < buttons.length; i++) { 
    if (buttons[i].isMouseOver()) {
      console.log(`Button text: ${buttons[i].text}`);
      console.log(`Button index: ${i}`);
      console.log('---');
      buttons[i].press();
      buttons[i].playSound();
    }
  }
}
